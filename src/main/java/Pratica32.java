/**
 *
 * @author nmessias
 */
public class Pratica32 {
    public static double densidade(double x, double media, 
            double desvio) {
        double elevado =  (-1.0/2) * (Math.pow((x - media) / desvio, 2));  
        double d = (1 / ((Math.sqrt(2 * Math.PI) * desvio)) * Math.pow(Math.E, elevado));
        return d;
    }
    
    public static void main(String[] args) {
        System.out.println(densidade(-1, 67, 3));
    } 
}
